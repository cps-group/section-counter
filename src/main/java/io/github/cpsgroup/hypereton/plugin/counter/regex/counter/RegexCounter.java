package io.github.cpsgroup.hypereton.plugin.counter.regex.counter;

import io.github.cpsgroup.hypereton.plugin.counter.regex.decomposer.TextDecomposer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Provides a method to extract from a {@link String} containing a compilation
 * of legal texts the number of legal texts, and respective sections and
 * subsections it contains.<br>
 * Those features are extracted according to descriptors matching the respective
 * structural document levels.<br>
 * The descriptors make use of regular expressions, see
 * {@link java.util.regex.Pattern} for further information on syntax and usage.
 *
 * @author Manuel Weidmann
 */
public class RegexCounter {

    String titleDescriptor, closureDescriptor,
            sectionDescriptor, subsectionDescriptor;
    
    private Logger logger = LoggerFactory.getLogger(RegexCounter.class);

    private TextDecomposer decomposer;

    /**
     * Creates a new {@link RegexCounter} to extract from a compilation of legal
     * texts the number of legal texts, and respective sections and subsections
     * it contains.
     *
     * @param titleDescriptor      A regular expression {@link String} denoting the beginning of
     *                             a legal text.
     * @param closureDescriptor    A regular expression {@link String} denoting the closing
     *                             comments of a legal text.
     * @param sectionDescriptor    A regular expression {@link String} denoting the beginning of
     *                             a section in a legal text.
     * @param subsectionDescriptor A regular expression {@link String} denoting the beginning of
     *                             a subsection in a legal text.
     * @author Manuel Weidmann
     */
    public RegexCounter(String titleDescriptor, String closureDescriptor,
                        String sectionDescriptor, String subsectionDescriptor) {

        this.titleDescriptor = titleDescriptor;
        this.closureDescriptor = closureDescriptor;
        this.sectionDescriptor = sectionDescriptor;
        this.subsectionDescriptor = subsectionDescriptor;

        decomposer = new TextDecomposer(titleDescriptor, closureDescriptor,
                sectionDescriptor, subsectionDescriptor);

    }

    /**
     * Returns the number of legal texts, sections and subsections contained in
     * this legal text compilation <br>
     * <b>Note</b>: Legal texts and sections that are not divided any further
     * count as having one section or subsection, respectively. The total number
     * of individual legal statements is thus equal to the number of
     * subsections.
     *
     * @param legalTextCompilation An HTML-formatted {@link String} containing a compilation of
     *                             legal texts.
     * @return An {@link int} array containing the number of respective
     * structure level elements found in the compilation of legal texts.
     * @author Manuel Weidmann
     */
    public int[] getTextStructure(String legalTextCompilation) {

        int numberOfLegalTexts = 0;
        int numberOfSections = 0;
        int numberOfSubsections = 0;

        /**
         * Structure the string according to the descriptors.
         */
        String[][][] decomposedText = decomposer
                .getDecomposedText(legalTextCompilation);

        /**
         * First level contains legal texts, i.e. arrays of sections.<br>
         * Second level contains sections, i.e. arrays of subsections.<br>
         * Third level contains subsections, i.e. arrays of text.
         */
        numberOfLegalTexts = decomposedText.length;

        for (String[][] legalText : decomposedText) {
            numberOfSections += legalText.length;
            for (String[] section : legalText) {
                if (section.length == 0) {
                    numberOfSubsections++;
                }
                numberOfSubsections += section.length;
            }
        }
        return new int[]{numberOfLegalTexts, Math.max(getNumberOfSections(legalTextCompilation), 0), numberOfSubsections};

    }

    public int getNumberOfSections(String legalTextCompilation) {

        String text, reference;
        text = legalTextCompilation;
        int numberOfSections = 0, numberOfSubsections = 0, numberOfNorms = 0, textFirst, textLast;
        ArrayList<Integer> sectionIndices, subsectionIndices, localSubsectionIndices;
        sectionIndices = new ArrayList<Integer>(10);
        subsectionIndices = new ArrayList<Integer>(50);
        localSubsectionIndices = new ArrayList<Integer>(10);

        Pattern titlePattern, closurePattern, sectionPattern, subsectionPattern, numberPattern, antiPattern;
        Matcher matcher;

        titlePattern = Pattern.compile(titleDescriptor);
        closurePattern = Pattern.compile(closureDescriptor);
        sectionPattern = Pattern.compile(sectionDescriptor);
        subsectionPattern = Pattern.compile(subsectionDescriptor);
        numberPattern = Pattern.compile("\\d+");
        antiPattern = Pattern.compile("(?m)^\\(\\d+\\)");


        /*
        index text
        replace text string with the relevant substring
         */
        matcher = titlePattern.matcher(text);
        boolean found = matcher.find();
        if (found) {
            textFirst = matcher.start();
        } else return numberOfSections;

        text = text.substring(textFirst);
        matcher = closurePattern.matcher(text);
        found = matcher.find();
        if (found) {
            while (matcher.start() <= textFirst) {
                found = matcher.find();
                if (!found) return numberOfSections;
            }
            textLast = matcher.start();
            text = text.substring(textFirst, textLast);
        } else return numberOfSections;

        /*
        index all sections
         */
        matcher = sectionPattern.matcher(text);
        while (matcher.find()) sectionIndices.add(matcher.start());

        /*
        index all subsections
         */
        matcher = subsectionPattern.matcher(text);
        while (matcher.find()) subsectionIndices.add(matcher.start());

        /*
        retrieve section count
        take either the last section or the total number of sections, whichever is higher
         */
        if (!sectionIndices.isEmpty()) {
            reference = text.substring(sectionIndices.get(sectionIndices.size() - 1));
            matcher = numberPattern.matcher(reference);
            if (matcher.find()) numberOfSections = Integer.parseInt(matcher.group());
//            numberOfSections = Math.max(sectionIndices.size(), numberOfSections);
        }

        /*
        retrieve subsection count
        count sections without subsections as a single subsection
         */
        int currentSubsectionIndex = 0, currentSectionIndex, numberOfLocalSubsections;

        for (currentSectionIndex = 0; currentSectionIndex < sectionIndices.size() - 1; currentSectionIndex++) {
            while (currentSubsectionIndex != subsectionIndices.size() && subsectionIndices.get(currentSubsectionIndex) < sectionIndices.get(currentSectionIndex))
                currentSubsectionIndex++;

            if (currentSubsectionIndex == subsectionIndices.size()) break;

            numberOfLocalSubsections = 0;
            localSubsectionIndices.clear();
            int index = subsectionIndices.get(currentSubsectionIndex);
            while (index < sectionIndices.get(currentSectionIndex + 1) && currentSubsectionIndex < subsectionIndices.size()) {
                index = subsectionIndices.get(currentSubsectionIndex);
                localSubsectionIndices.add(index);
                currentSubsectionIndex++;
            }
            /*
            retrieve subsection count
            take either the last subsection or the total number of subsections or 1, whichever is higher
            */
            if (!localSubsectionIndices.isEmpty()) {
                reference = text.substring(localSubsectionIndices.get(localSubsectionIndices.size() - 1));
                matcher = antiPattern.matcher(reference);
                if (!matcher.find() /*|| matcher.start() < textFirst*/) {
                    matcher = numberPattern.matcher(reference);
                    if (matcher.find()) numberOfLocalSubsections = Integer.parseInt(matcher.group());
                    String match = matcher.group();
//                    logger.info("Found local subsections:\t" + numberOfLocalSubsections);
                    numberOfLocalSubsections = Math.max(localSubsectionIndices.size(), numberOfLocalSubsections);
                    numberOfSubsections += numberOfLocalSubsections;
                    numberOfNorms += numberOfLocalSubsections;
                } else numberOfNorms++;
            } else numberOfNorms++;
        }
        numberOfNorms += numberOfSections - currentSectionIndex - 1;

//        logger.info("\tSECTIONS\tSUBSECTIONS\tNORMS");
//        logger.info("\t" + numberOfSections + "\t\t" + numberOfSubsections + "\t\t" + numberOfNorms);


        return numberOfNorms;
    }

}
