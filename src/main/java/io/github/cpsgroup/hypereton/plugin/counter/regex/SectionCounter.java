package io.github.cpsgroup.hypereton.plugin.counter.regex;

import com.google.gson.Gson;
import io.github.cpsgroup.hypereton.plugin.counter.Counter;
import io.github.cpsgroup.hypereton.plugin.counter.regex.counter.RegexCounter;
import io.github.cpsgroup.hypereton.plugin.counter.regex.parser.PDFParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class SectionCounter implements Counter {

    private String file, titleDescriptor, closureDescriptor, sectionDescriptor,
            subsectionDescriptor;
    private int startingPage, endPage;
    private boolean removeQuotes;
    private PDFParser parser;
    private final File JSON_CONFIG = new File("sectioncounter.json");

    private Logger logger = LoggerFactory.getLogger(SectionCounter.class);

    public SectionCounter() {
        startingPage = 1;
        endPage = Integer.MAX_VALUE;
        removeQuotes = false;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(JSON_CONFIG));
            Gson gson = new Gson();
            Map<String, String> map = gson.fromJson(reader, Map.class);
            configure(map);
        } catch (FileNotFoundException e) {
        }

    }

    public SectionCounter(String[] args) throws IllegalArgumentException {
        if (args.length < 5) {
            throw new IllegalArgumentException(
                    "Did not sufficiently specify input parameters!");
        }

        file = args[4];
        startingPage = 1;
        endPage = Integer.MAX_VALUE;

        titleDescriptor = args[0];
        closureDescriptor = args[1];
        sectionDescriptor = args[2];
        subsectionDescriptor = args[3];

        try {
            if (args.length >= 7) {
                startingPage = Integer.parseInt(args[5]);
                endPage = Integer.parseInt(args[6]);
            }
            if (args.length == 6) {
                removeQuotes = Boolean.parseBoolean(args[5]);
            }
            if (args.length == 8) {
                removeQuotes = Boolean.parseBoolean(args[7]);
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(
                    "Invalid arguments for page range; only positive non-zero integers allowed.");
        }
    }

    @Override
    public int getNumberOfSections() {
        parser = new PDFParser();
        String content = "";
        int sections = 0;

        try {
            Path path = Paths.get(file);
            if (Files.isDirectory(path)) {
                try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path)) {
                    for (Path p : directoryStream) {
                        logger.info("Processing file '" + p.getFileName() + "' in " + path);
                        String[] args = {titleDescriptor, closureDescriptor, sectionDescriptor,
                                subsectionDescriptor, p.toString()};
                        new SectionCounter(args).getNumberOfSections();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                    return -1;
                }
                return -2;
            }

            content = parser.getContent(file, startingPage, endPage,
                    removeQuotes);

            int[] structure = new RegexCounter(titleDescriptor, closureDescriptor,
                    sectionDescriptor, subsectionDescriptor)
                    .getTextStructure(content);
//            logger.info("Number of legal texts:\t" + structure[0]);
//            logger.info("Number of sections:\t" + structure[1]);
//            logger.info("Number of subsections:\t" + structure[2]);

            LinkedHashMap<String, Integer> structuredContent = new LinkedHashMap<>();
            structuredContent.put("texts", structure[0]);
            structuredContent.put("sections", structure[1]);
            structuredContent.put("subsections", structure[2]);

            Gson gson = new Gson();
            String jsonContent = gson.toJson(structuredContent);
            if (!Files.exists(Paths.get("json"))) Files.createDirectory(Paths.get("json"));
            Files.deleteIfExists(Paths.get("json" + File.separator + path.getFileName().toString().split("\\.")[0] +
                    ".json"));
            Files.write(Paths.get("json" + File.separator + path.getFileName().toString().split("\\.")[0] + ".json"),
                    jsonContent.getBytes());

            sections = structure[1];

        } catch (IOException e) {
            System.out
                    .println("Unable to access file. Make sure the path points to a valid unencrypted pdf file that " +
                            "is not locked by another program.");
            e.printStackTrace();
            return -1;
        }
        return sections;
    }

    @Override
    public int getNumberOfPages() {
        try {
            return parser.getNumberOfPages();
        } catch (IOException e) {
            return 0;
        }
    }

    @Override
    public int getNumberOfWords() {
        return parser.getNumberOfWords();
    }

    @Override
    public void configure(Map<String, String> parameters) {
        if (parameters.containsKey("file")) file = parameters.get("file");
        if (parameters.containsKey("titleDescriptor")) titleDescriptor = parameters.get("titleDescriptor");
        if (parameters.containsKey("closureDescriptor")) closureDescriptor = parameters.get("closureDescriptor");
        if (parameters.containsKey("sectionDescriptor")) sectionDescriptor = parameters.get("sectionDescriptor");
        if (parameters.containsKey("subsectionDescriptor"))
            subsectionDescriptor = parameters.get("subsectionDescriptor");
        if (parameters.containsKey("startPage")) startingPage = Integer.parseInt(parameters.get("startPage"));
        if (parameters.containsKey("endPage")) endPage = Integer.parseInt(parameters.get("endPage"));
        if (parameters.containsKey("removeQuotes")) removeQuotes = Boolean.parseBoolean(parameters.get("startPage"));
    }

    @Override
    public String getName() {
        return "Section Counter";
    }

    @Override
    public String getDescription() {
        return "The basic section counter for the Hypereton project.\n\nConfiguration parameters:" + "\n\tfile" +
                "\n\ttitleDescriptor" + "\n\tclosureDescriptor" + "\n\tsectionDescriptor" +
                "\n\tsubsectionDescriptor" + "\n\tstartPage" +
                "\n\tendPage" +
                "\n\tremoveQuotes";
    }
}
