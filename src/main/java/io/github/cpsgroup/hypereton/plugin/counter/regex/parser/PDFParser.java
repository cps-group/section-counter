package io.github.cpsgroup.hypereton.plugin.counter.regex.parser;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.util.PDFTextStripperByArea;

import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

/**
 * Provides methods to read in PDF files with text components and try to
 * preserve basic formatting by the use of HTML encoding.
 *
 * @author Manuel Weidmann
 */
public class PDFParser {

    // private final char QUOTE_START1 = '\u201e';
    // private final char QUOTE_START2 = '\u201a';
    // private final char QUOTE_START3 = '\u2039';
    // private final char QUOTE_START4 = '\u00bb';
    // private final char QUOTE_START5 = '>';

    private final char[] OPENING_QUOTES = new char[]{'\u201e', '\u201a',
            '\u203a', '\u00bb', '>'};

    // private final char QUOTE_END1 = '\u201c';
    // private final char QUOTE_END2 = '\u2018';
    // private final char QUOTE_END3 = '\u203a';
    // private final char QUOTE_END4 = '\u00ab';
    // private final char QUOTE_END5 = '<';

    private final char[] CLOSING_QUOTES = new char[]{'\u201c', '\u2018',
            '\u2039', '\u00ab', '<'};

    private final HashMap<Character, Integer> OPENING = new HashMap<Character, Integer>(
            5);
    private final HashMap<Character, Integer> CLOSING = new HashMap<Character, Integer>(
            5);

    private int numberOfPages = 0;
    private int numberOfWords = 0;

    public PDFParser() {
        // OPENING.put(QUOTE_START1, 1);
        // OPENING.put(QUOTE_START2, 2);
        // OPENING.put(QUOTE_START3, 3);
        // OPENING.put(QUOTE_START4, 4);
        // OPENING.put(QUOTE_START5, 5);
        //
        // CLOSING.put(QUOTE_END1, 1);
        // CLOSING.put(QUOTE_END2, 2);
        // CLOSING.put(QUOTE_END3, 3);
        // CLOSING.put(QUOTE_END4, 4);
        // CLOSING.put(QUOTE_END5, 5);

        int i = 0;
        for (char c : OPENING_QUOTES) {
            OPENING.put(c, i);
        }
        i = 0;
        for (char c : CLOSING_QUOTES) {
            CLOSING.put(c, i);
        }
    }

    public int getNumberOfPages() throws IOException {
        return numberOfPages;
    }

    public int getNumberOfWords(){
        return numberOfWords;
    }

    /**
     * @param file         A {@link String} denoting the path to the PDF file which is to
     *                     be parsed.
     * @param startingPage The page from which to start parsing.
     * @param endPage      The page after which to stop parsing.
     * @param removeQuotes Whether to remove all quoted text or not.
     * @return An HTML encoded {@link String} containing the specified portion
     * of the PDF file.
     * @throws IOException If reading from the PDF file fails at any point.
     * @author Manuel Weidmann
     */
    public String getContent(String file, int startingPage, int endPage,
                             boolean removeQuotes) throws IOException {
        StringBuffer content = new StringBuffer("");

        PDDocument pdf;
        try {
            pdf = PDDocument.load(file);
        } catch (IOException e) {
            System.out.println("Error loading " + file
                    + ", not a valid PDF file!");
            e.printStackTrace();
            throw e;
        }

        numberOfPages = pdf.getNumberOfPages();

        try {
            if (!Files.exists(FileSystems.getDefault().getPath(
                    "ocr" + FileSystems.getDefault().getSeparator()
                            + file.split("\\.")[0]
                            + FileSystems.getDefault().getSeparator()
                            + file.split("\\.")[0] + "-" + 0 + ".png"))) {
                toImage(file, startingPage, endPage);
            }

            for (int i = startingPage; i <= pdf.getNumberOfPages(); i++) {
                // content.append(getContentFromPage(file, i, removeQuotes));
                content.append(getOCRContentFromPage(file, i));
            }

            numberOfWords = getWordCount(content.toString());

            if (removeQuotes) {
                return getTextWithoutQuotes(content.toString());
            } else {
                return preprocess(content.toString());
            }
//            return content.toString();
        } catch (IOException e) {
            throw e;
        } catch (IndexOutOfBoundsException e) {
            return content.toString();
        } finally {
            if (pdf != null) pdf.close();
        }
    }

    private int getWordCount(String text){
        String trim = text.trim();
        if (trim.isEmpty()) return 0;
        return trim.split("\\W+").length; //separate string around spaces
    }

    @Deprecated
    private String getContentFromPage(String file, int page,
                                      boolean removeQuotes) throws IOException {
        /**
         * Create empty string to return at least a valid value even if no text
         * was extracted.
         */
        String content = "";

        /**
         * Try to access specified file location.
         */
        File in = new File(file);
        try {
            if (!in.exists()) {
                throw new IOException();
            }
        } catch (IOException e) {
            System.out.println("Error accessing " + file
                    + ", file does not exist!");
            throw e;
        }

        // /**
        // * Lock file access to prevent concurrent modification, also check if
        // * file is already locked by another process.
        // */
        // FileChannel channel = null;
        // try {
        // channel = new RandomAccessFile(file, "rw").getChannel();
        // if (channel.tryLock() == null) {
        // throw new IOException();
        // }
        // } catch (OverlappingFileLockException e) {
        // System.out.println("Error accessing " + file + ", file is locked!");
        // throw e;
        // } catch (IOException e) {
        // System.out.println("Error accessing " + file
        // + ", file can not be read from!");
        // throw e;
        // } finally {
        // if (channel instanceof FileChannel) {
        // channel.close();
        // }
        // }

        /**
         * Let PDFbox ingest the file.
         */
        PDDocument pdf;
        try {
            pdf = PDDocument.load(in);
        } catch (IOException e) {
            System.out.println("Error loading " + file
                    + ", not a valid PDF file!");
            throw e;
        }

        /**
         * Prepare for text extraction: Create a 'to HTML' - text stripper; use
         * UTF-encoding.
         */
        PDFTextStripperByArea stripper;
        try {
            stripper = new PDFTextStripperByArea("utf-8");
        } catch (IOException e) {
            System.out.println("Error initialising text stripper!");
            throw e;
        }

        /**
         * Perform some sanity checks on starting and end page inputs.
         */
        // if (startingPage > 0 && startingPage <= endPage
        // && startingPage <= pdf.getNumberOfPages()) {
        // stripper.setStartPage(startingPage);
        // }
        // if (endPage > 0 && endPage >= startingPage
        // && endPage <= pdf.getNumberOfPages()) {
        // stripper.setEndPage(endPage);
        // }

        /**
         * Store extracted text.
         */
        try {
            List pageList = pdf.getDocumentCatalog().getAllPages();
            Object[] pages = pageList.toArray();
            System.out.println(pages.length);
            PDRectangle rect = ((PDPage) pages[page]).findMediaBox();
            //System.out.println(rect);

            //System.out.println("extracting from page " + page);

            Rectangle2D.Float left = new Rectangle2D.Float(0, 0,
                    rect.getWidth() * 0.5f, rect.getHeight());
            stripper.addRegion("column_left  " + page, left);
            //System.out.println(left);

            Rectangle2D.Float right = new Rectangle2D.Float(
                    rect.getWidth() * 0.5f, 0, rect.getWidth() * 0.5f,
                    rect.getHeight());
            stripper.addRegion("column_right " + page, right);
            //System.out.println(right);

            stripper.extractRegions((PDPage) pages[page]);

            List<String> regions = stripper.getRegions();
            for (String region : regions) {
                //System.out.println(region);
            }
            //System.out.println(stripper
            //        .getTextForRegion("column_left  " + page)
            //        + "########################################");
            //System.out.println(stripper
            //        .getTextForRegion("column_right " + page));
            content += stripper.getTextForRegion("column_left  " + page)
                    // + "\n\n\n######################################################"
                    + stripper.getTextForRegion("column_right " + page)
            // + "\n\n\n######################################################";
            ;

            // List<String> regions = stripper.getRegions();
            // for (String region : regions) {
            // content = content + stripper.getTextForRegion(region);
            // System.out.println("\n\n\n");
            // System.out.println("COLUMN " + region + " START");
            // System.out
            // .println("######################################################");
            // System.out.println(stripper.getTextForRegion(region));
            // System.out
            // .println("######################################################");
            // System.out.println("COLUMN " + region + " END");
            // System.out.println("\n\n\n");
            // }
            // regions.clear();

            // content = stripper.getText(pdf);
        } catch (IOException e) {
            System.out.println("Error extracting text from" + file
                    + ", invalid doc state or file is encrypted!");
            throw e;
        }

        /**
         * Close in-memory document.
         */
        pdf.close();

        /**
         * If the removal of quotes is requested, comply to do so.
         */
        System.out.println("Remove quotes? " + removeQuotes);
        if (removeQuotes) {
            return getTextWithoutQuotes(content);
        } else {
            return content;
        }
    }

    private void toImage(String file, int startingPage, int endPage)
            throws IOException {
        Path path = FileSystems.getDefault().getPath(
                "ocr" + FileSystems.getDefault().getSeparator()
                        + file.split("\\.")[0]);
        Files.createDirectories(path);
        path = FileSystems.getDefault().getPath(
                "ocr" + FileSystems.getDefault().getSeparator()
                        + file.split("\\.")[0]
                        + FileSystems.getDefault().getSeparator()
                        + FileSystems.getDefault().getPath(file).getFileName().toString().split("\\.")[0]);

        if (Files.exists(Paths.get(path.toString() + ".png")) || Files.exists(Paths.get(path.toString() + "-1.png"))) {
            return;
        }

//        ProcessBuilder pb = new ProcessBuilder(
//                "util\\imagemagick\\convert.exe", "-density", "300", "-depth",
//                "8", "-quality", "85", "-median", "2", file, path.toString() + ".png");
//        System.out.println("util\\imagemagick\\convert.exe" + " -density"
//                + " 300" + " -depth" + " 8" + " -quality" + " 85" +" -median" +" 2 " + file+" "
//                + path.toString() + ".png");
        ProcessBuilder pb = new ProcessBuilder("util\\gs\\bin\\gswin32c.exe", "-o", path.toString() + "-%d.png", "-sDEVICE=pngalpha", "-r300", file);
        Process p = pb.start();
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // /**
        // * Try to access specified file location.
        // */
        // File in = new File(file);
        // try {
        // if (!in.exists()) {
        // throw new IOException();
        // }
        // } catch (IOException e) {
        // System.out.println("Error accessing " + file
        // + ", file does not exist!");
        // throw e;
        // }
        //
        // /**
        // * Let PDFbox ingest the file.
        // */
        // PDDocument pdf;
        // try {
        // pdf = PDDocument.load(in);
        // } catch (IOException e) {
        // System.out.println("Error loading " + file
        // + ", not a valid PDF file!");
        // throw e;
        // }
        //
        // Path path = FileSystems.getDefault().getPath(
        // "images" + FileSystems.getDefault().getSeparator()
        // + file.split("\\.")[0]);
        // Files.createDirectories(path);
        // new PDFImageWriter().writeImage(pdf, "png", null, 1,
        // Integer.MAX_VALUE,
        // path.toString() + FileSystems.getDefault().getSeparator()
        // + file.split("\\.")[0] + "-",
        // BufferedImage.TYPE_INT_RGB, 300);
    }

    private String getOCRContentFromPage(String file, int page)
            throws IOException {

//        System.out.println(file);
        try {
            Path path = FileSystems.getDefault().getPath(
                    "ocr" + FileSystems.getDefault().getSeparator()
                            + file.split("\\.")[0]
                            + FileSystems.getDefault().getSeparator()
                            + FileSystems.getDefault().getPath(file).getFileName().toString().split("\\.")[0] + "-" + page);

//            System.out.println(path);

            if (!Files.exists(FileSystems.getDefault().getPath(
                    "ocr" + FileSystems.getDefault().getSeparator()
                            + file.split("\\.")[0]
                            + FileSystems.getDefault().getSeparator()
                            + FileSystems.getDefault().getPath(file).getFileName().toString().split("\\.")[0] + "-" + page + ".txt"))) {

                if (!Files.exists(FileSystems.getDefault().getPath(
                        "ocr" + FileSystems.getDefault().getSeparator()
                                + file.split("\\.")[0]
                                + FileSystems.getDefault().getSeparator()
                                + FileSystems.getDefault().getPath(file).getFileName().toString().split("\\.")[0] + "-" + page + ".png"))) {
                    path = FileSystems.getDefault().getPath(
                            "ocr" + FileSystems.getDefault().getSeparator()
                                    + file.split("\\.")[0]
                                    + FileSystems.getDefault().getSeparator()
                                    + FileSystems.getDefault().getPath(file).getFileName().toString().split("\\.")[0]);
                    ProcessBuilder pb = new ProcessBuilder(
                            "util\\tesseract\\tesseract", path.toString() + ".png",
                            path.toString() + "-0", "-l", "dan");
                    Process p = pb.start();
                    p.waitFor();
                } else {
                    ProcessBuilder pb = new ProcessBuilder(
                            "util\\tesseract\\tesseract", path.toString() + ".png",
                            path.toString(), "-l", "dan");
                    Process p = pb.start();
                    p.waitFor();
                }

            }

            path = FileSystems.getDefault().getPath(
                    "ocr" + FileSystems.getDefault().getSeparator()
                            + file.split("\\.")[0]
                            + FileSystems.getDefault().getSeparator()
                            + FileSystems.getDefault().getPath(file).getFileName().toString().split("\\.")[0] + "-" + page + ".txt");

            BufferedReader reader = Files.newBufferedReader(path,
                    Charset.defaultCharset());
            StringBuilder content = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                content.append(line).append(
                        System.getProperty("line.separator"));
            }

//            System.out.println(content);

            return content.toString();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * @param content A {@link String} containing text with proper quotes, i.e. each
     *                opening quotation mark is paired with a corresponding closing
     *                one.
     * @return The input {@link String} with all occurrences of quoted text
     * removed.
     * @author Manuel Weidmann
     */
    private String getTextWithoutQuotes(String content) {

        content = preprocess(content);
//        System.out.println(content);

//        System.out.println("Original length:\t" + content.length());
        /**
         * Create Stringbuffer to write to and initialise open and close quote
         * counts to 0
         */
        StringBuffer buffer = new StringBuffer("");
        // assume the text to be part of a quote started on a previous page
        int openQuotes = 0;
        // int quoteStart = 0;

        // /**
        // * If the string is shorter than 6 characters it cannot contain an
        // HTML
        // * encoded quotation mark; simply return the string in this case.
        // */
        // if (content.length() < 7) {
        // return content;
        // }
        //
        // /**
        // * Read in the first 6 characters.
        // */
        // for (int i = 0; i < 7; i++) {
        // buffer.append(content.charAt(i));
        // }
        //
        // /**
        // * Read in character after character and check for HTML encoded
        // * quotation marks. Count opening and closing ones, remove quotation
        // * marks and the text between first opening and corresponding closing
        // * one if such a pair is found.
        // */
        // for (int i = 7; i < content.length(); i++) {
        // buffer.append(content.charAt(i));
        //
        // if (buffer.substring(buffer.length() - 7, buffer.length()).equals(
        // QUOTE_START1)
        // || buffer.substring(buffer.length() - 7, buffer.length())
        // .equals(QUOTE_START2)
        // || buffer.substring(buffer.length() - 7, buffer.length())
        // .equals(QUOTE_START3)
        // || buffer.substring(buffer.length() - 7, buffer.length())
        // .equals(QUOTE_START4)) {
        // if (openQuotes == 0) {
        // quoteStart = buffer.length() - 7;
        // }
        // openQuotes++;
        //
        // } else if (openQuotes > 0
        // && buffer.substring(buffer.length() - 7, buffer.length())
        // .equals(QUOTE_END1)
        // || buffer.substring(buffer.length() - 7, buffer.length())
        // .equals(QUOTE_END2)
        // || buffer.substring(buffer.length() - 7, buffer.length())
        // .equals(QUOTE_END3)
        // || buffer.substring(buffer.length() - 7, buffer.length())
        // .equals(QUOTE_END4)) {
        // openQuotes--;
        // if (openQuotes == 0) {
        // buffer.replace(quoteStart, buffer.length(), "");
        // }
        // }
        //
        // }

        char lastOpening = ' ';
        for (int i = 0; i < content.length(); i++) {
            char c = content.charAt(i);
//            if (CLOSING.containsKey(c) && CLOSING.get(c) == OPENING.get(lastOpening)) {
//                buffer.append(c);
//            }
//            else
            if (OPENING.containsKey(c)) {
                // System.out.println("Found an opening quote: "
                // + c);
                if (openQuotes == -1) {
                    openQuotes = 1;
                } else {
                    openQuotes += 1;
                }
                lastOpening = c;
            } else if (CLOSING.containsKey(c)) {
                // System.out.println("Found a closing quote: "
                // + c);

                openQuotes -= 1;
                if (openQuotes < 0) {
                    openQuotes = 0;
                    buffer = new StringBuffer("");
                }

            } else if (openQuotes == 0) {
                buffer.append(c);
            }
        }

        content = buffer.toString();

//        System.out.println(content);

        // content = content.replaceAll(QUOTE_START1 + ".*?" + QUOTE_END1, "");
        // content = content.replaceAll(QUOTE_START2 + ".*?" + QUOTE_END2, "");
        // content = content.replaceAll(QUOTE_START3 + ".*?" + QUOTE_END3, "");
        // content = content.replaceAll(QUOTE_START4 + ".*?" + QUOTE_END4, "");

//        System.out.println("Final length:\t\t" + content.length());

        return content;
        // return buffer.toString();
    }

    private String preprocess(String content) {

        /**
         * Preprocess quotes
         */
        String preprocessed = content.replaceAll(",,", "„");
        preprocessed = preprocessed.replaceAll("((\"\")|(\\w\"))", "“");
        preprocessed = preprocessed.replaceAll(" \"", "„");
        preprocessed = preprocessed.replace(" . ", " ");

        return preprocessed;
    }
}
