package io.github.cpsgroup.hypereton.sectioncounter.decomposer;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import io.github.cpsgroup.hypereton.plugin.counter.regex.decomposer.TextDecomposer;
import org.junit.Before;
import org.junit.Test;

import io.github.cpsgroup.hypereton.plugin.counter.regex.parser.PDFParser;

public class TextDecomposerTest {

	private final String PATH_PREFIX = "src" + File.separator + "test"
			+ File.separator + "resources" + File.separator + "decomposer"
			+ File.separator;

	PDFParser parser;
	TextDecomposer decomposer;

	@Before
	public void init() {
		decomposer = new TextDecomposer("title descriptor",
				"closure descriptor", "[^b]section descriptor",
				"subsection descriptor");
		parser = new PDFParser();
	}

	/*
	 * Test constructor
	 */
	@Test
	public void testTextDecomposer() {
		assertTrue(decomposer instanceof TextDecomposer);
	}

	/*
	 * Test title decomposition
	 */
	@Test
	public void testGetLegalTextsSingle() {
		String[] legalTexts = null;
		try {
			legalTexts = decomposer.getLegalTexts(parser.getContent(PATH_PREFIX
					+ "legalTextSingle.pdf", 0, 0, true));
		} catch (IOException e) {
			fail("Exception thrown!");
		}
		if (!(legalTexts instanceof String[])) {
			fail("Failed to decompose into legal texts!");
		}
		assertTrue("Expected 1 text but found " + legalTexts.length
				+ " instead.", legalTexts.length == 1);
	}

	@Test
	public void testGetLegalTextsZero() {
		String[] legalTexts = null;
		try {
			legalTexts = decomposer.getLegalTexts(parser.getContent(PATH_PREFIX
					+ "legalTextZero.pdf", 0, 0, true));
		} catch (IOException e) {
			fail("Exception thrown!");
		}
		if (!(legalTexts instanceof String[])) {
			fail("Failed to decompose into legal texts!");
		}
		assertTrue("Expected no text but found " + legalTexts.length
				+ " instead.", legalTexts.length == 0);
	}

	@Test
	public void testGetLegalTextsArtifact() {
		String[] legalTexts = null;
		try {
			legalTexts = decomposer.getLegalTexts(parser.getContent(PATH_PREFIX
					+ "legalTextArtifact.pdf", 0, 0, true));
		} catch (IOException e) {
			fail("Exception thrown!");
		}
		if (!(legalTexts instanceof String[])) {
			fail("Failed to decompose into legal texts!");
		}
		assertTrue("Expected 1 text but found " + legalTexts.length
				+ " instead.", legalTexts.length == 1);
	}

	@Test
	public void testGetLegalTextsMulti() {
		String[] legalTexts = null;
		try {
			legalTexts = decomposer.getLegalTexts(parser.getContent(PATH_PREFIX
					+ "legalTextMulti.pdf", 0, 0, true));
		} catch (IOException e) {
			fail("Exception thrown!");
		}
		if (!(legalTexts instanceof String[])) {
			fail("Failed to decompose into legal texts!");
		}
		assertTrue("Expected 3 texts but found " + legalTexts.length
				+ " instead.", legalTexts.length == 3);
	}

	/*
	 * Test section decomposition
	 */
	@Test
	public void testGetSectionsSingle() {
		String[] sections = null;
		try {
			sections = decomposer.getSections(parser.getContent(PATH_PREFIX
					+ "sectionSingle.pdf", 0, 0, true));
		} catch (IOException e) {
			fail("Exception thrown!");
		}
		if (!(sections instanceof String[])) {
			fail("Failed to decompose into sections!");
		}
		if (!(sections instanceof String[])) {
			fail("Failed to decompose into sections!");
		}
		if (!(sections instanceof String[])) {
			fail("Failed to decompose into sections!");
		}
		assertTrue("Expected 1 section but found " + sections.length
				+ " instead.", sections.length == 1);
	}

	@Test
	public void testGetSectionsMulti() {
		String[] sections = null;
		try {
			sections = decomposer.getSections(parser.getContent(PATH_PREFIX
					+ "sectionMulti.pdf", 0, 0, true));
		} catch (IOException e) {
			fail("Exception thrown!");
		}
		if (!(sections instanceof String[])) {
			fail("Failed to decompose into sections!");
		}
		assertTrue("Expected 6 sections but found " + sections.length
				+ " instead.", sections.length == 6);
	}

	/*
	 * Test subsection decomposition
	 */
	@Test
	public void testGetSubsectionsSingle() {
		String[] subsections = null;
		try {
			subsections = decomposer.getSubsections(parser.getContent(
					PATH_PREFIX + "subsectionSingle.pdf", 0, 0, true));
		} catch (IOException e) {
			fail("Exception thrown!");
		}
		if (!(subsections instanceof String[])) {
			fail("Failed to decompose into subsections!");
		}
		assertTrue("Expected 1 subsection but found " + subsections.length
				+ " instead.", subsections.length == 1);
	}

	@Test
	public void testGetSubsectionsMulti() {
		String[] subsections = null;
		try {
			subsections = decomposer.getSubsections(parser.getContent(
					PATH_PREFIX + "subsectionMulti.pdf", 0, 0, true));
		} catch (IOException e) {
			fail("Exception thrown!");
		}
		if (!(subsections instanceof String[])) {
			fail("Failed to decompose into subsections!");
		}
		assertTrue("Expected 6 subsections but found " + subsections.length
				+ " instead.", subsections.length == 6);
	}

	/*
	 * Test document decomposition
	 */
	@Test
	public void testGetDecomposedText() {

		String[][][] decomposedText = null;
		String content = "";
		try {
			content = parser.getContent(PATH_PREFIX
					+ "legalTextCompilation.pdf", 0, 0, true);
			decomposedText = decomposer.getDecomposedText(content);
		} catch (IOException e) {
			fail("Exception thrown!");
		}
		if (!(decomposedText instanceof String[][][])) {
			fail("Failed to decompose document!");
		}
		assertTrue("Expected 3 texts but found " + decomposedText.length
				+ " instead.", decomposedText.length == 3);

		assertTrue("Expected 6 sections but found " + decomposedText[0].length
				+ " instead.", decomposedText[0].length == 6);
		assertTrue("Expected 6 subsections but found "
				+ decomposedText[0][0].length + " instead.",
				decomposedText[0][0].length == 6);
		assertTrue("Expected 6 subsections but found "
				+ decomposedText[0][1].length + " instead.",
				decomposedText[0][1].length == 6);
		assertTrue("Expected 6 subsections but found "
				+ decomposedText[0][2].length + " instead.",
				decomposedText[0][2].length == 6);
		assertTrue("Expected 6 subsections but found "
				+ decomposedText[0][3].length + " instead.",
				decomposedText[0][3].length == 6);
		assertTrue("Expected 6 subsections but found "
				+ decomposedText[0][4].length + " instead.",
				decomposedText[0][4].length == 6);
		assertTrue("Expected 6 subsections but found "
				+ decomposedText[0][5].length + " instead.",
				decomposedText[0][5].length == 6);

		assertTrue("Expected 1 section but found " + decomposedText[1].length
				+ " instead.", decomposedText[1].length == 1);
		assertTrue(decomposedText[1][0].length == 1);

		assertTrue("Expected 6 sections but found " + decomposedText[2].length
				+ " instead.", decomposedText[2].length == 6);

		assertTrue("Expected 6 subsections but found "
				+ decomposedText[2][5].length + " instead.",
				decomposedText[2][0].length == 6);
		assertTrue("Expected 6 subsections but found "
				+ decomposedText[2][5].length + " instead.",
				decomposedText[2][1].length == 6);
		assertTrue("Expected 6 subsections but found "
				+ decomposedText[2][5].length + " instead.",
				decomposedText[2][2].length == 6);
		assertTrue("Expected 6 subsections but found "
				+ decomposedText[2][5].length + " instead.",
				decomposedText[2][3].length == 6);
		assertTrue("Expected 6 subsections but found "
				+ decomposedText[2][5].length + " instead.",
				decomposedText[2][4].length == 1);
		assertTrue("Expected 6 subsections but found "
				+ decomposedText[2][5].length + " instead.",
				decomposedText[2][5].length == 1);

		for (String[][] legalText : decomposedText) {
			for (String[] section : legalText) {
				for (String subsection : section) {
					assertTrue("Expected to find text 'body ' but found "
							+ subsection + " instead.",
							subsection.contains("body "));
				}
			}
		}
	}
}
