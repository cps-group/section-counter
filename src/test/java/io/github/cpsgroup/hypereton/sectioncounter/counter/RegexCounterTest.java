package io.github.cpsgroup.hypereton.sectioncounter.counter;

import io.github.cpsgroup.hypereton.plugin.counter.regex.counter.RegexCounter;
import io.github.cpsgroup.hypereton.plugin.counter.regex.parser.PDFParser;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class RegexCounterTest {

	private final String PATH_PREFIX = "src" + File.separator + "test"
			+ File.separator + "resources" + File.separator + "counter"
			+ File.separator;

	PDFParser parser;
	RegexCounter regexCounter;

	@Before
	public void init() {
		regexCounter = new RegexCounter("title descriptor", "closure descriptor",
				"[^b]section descriptor", "subsection descriptor");
	}

	@Test
	public void testCounter() {
		assertTrue(regexCounter instanceof RegexCounter);
	}

	@Test
	public void testGetTextStructure() {
		parser = new PDFParser();
		String content = "";
		try {
			content = parser.getContent(PATH_PREFIX
					+ "legalTextCompilation.pdf", 0, 0, true);
		} catch (IOException e) {
			fail("Exception thrown!");
		}
		int[] structureLevels = regexCounter.getTextStructure(content);

		assertTrue("Expected 3 legal texts but got " + structureLevels[0]
				+ " instead.", structureLevels[0] == 3);
		assertTrue("Expected 13 sections but got " + structureLevels[1]
				+ " instead.", structureLevels[1] == 13);
		assertTrue("Expected 63 subsections but got " + structureLevels[2]
				+ " instead.", structureLevels[2] == 63);
	}
}
